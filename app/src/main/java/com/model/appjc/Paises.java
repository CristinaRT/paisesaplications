package com.model.appjc;

public class Paises {

    private int codigo;
    private String nombre;
    private String fotoURL;
    private String videoURL;


    public Paises() {
    }

    public Paises(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Paises(int codigo, String nombre, String fotoURL) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.fotoURL = fotoURL;
    }

    public Paises(int codigo, String nombre, String fotoURL, String videoURL) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.fotoURL = fotoURL;
        this.videoURL = videoURL;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getFotoURL() {
        return fotoURL;
    }

    public void setFotoURL(String fotoURL) {
        this.fotoURL = fotoURL;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
