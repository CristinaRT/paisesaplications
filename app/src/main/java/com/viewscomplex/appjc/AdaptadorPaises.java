package com.viewscomplex.appjc;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appjc.R;
import com.model.appjc.Paises;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.List;


public class AdaptadorPaises extends RecyclerView.Adapter<AdaptadorPaises.MyViewHolder> {
    private List<Paises> paises;

    // Provee una referencia a la vista para cada item de datos
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // cada item de datos es solo una cadena para este ejemplo
        public TextView textViewProducto;
        public ImageView imageViewFoto;
        public YouTubePlayerView  videoViewYoutube;


        public MyViewHolder(View v) {
            super(v);

            textViewProducto = v.findViewById(R.id.textViewProducto);
            imageViewFoto = v.findViewById(R.id.imageViewFoto);
            videoViewYoutube = v.findViewById(R.id.playerViewYoutube);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdaptadorPaises(List<Paises> myDataset) {
        paises = myDataset;
    }

    @Override
    public AdaptadorPaises.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.complex_row_item, parent, false);

        return new AdaptadorPaises.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorPaises.MyViewHolder holder, final int position) {
        final Paises p = paises.get(position);
        holder.textViewProducto.setText(p.getNombre());
        Glide.with(holder.itemView.getContext()).load(p.getFotoURL()).into(holder.imageViewFoto);
        holder.videoViewYoutube.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);
                Log.e("video",p.getVideoURL());
                youTubePlayer.loadVideo(p.getVideoURL(),0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paises.size();
    }
}

